.. SSS documentation master file, created by
   sphinx-quickstart on Fri Jul 14 12:20:37 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: ./images/mass_placeholder.png
  :scale: 40 %
   
Scenario Studio for SCRIMMAGE (SSS) Documentation
=======================================================

.. toctree::
  :maxdepth: 2
  :caption: Table of Contents:
   
  doc-files/getting-started/getting_started
  doc-files/overview/overview
  doc-files/docs/docs
  doc-files/tutorials/tutorials
  doc-files/demonstrations/demonstrations
  doc-files/resources/resources
  
..