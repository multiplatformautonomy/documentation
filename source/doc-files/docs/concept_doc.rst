Basic Concepts
======================

This section will focus on explaining SCRIMMAGE, and how to access and use it
in the Scenario Studio at a conceptual level. This will explain what plugins
are and the different types of plugins available (sensor, controller, etc.).
We may also cover the publisher/subscriber model and the parameters if necessary.

This section will also cover navigation in the user interface of Scenario
studio, as well as how to configure the environment to perform multiple runs
on a local computer or setup to run on available clusters. Other utilities will
also be covered near the end of this section.


Plugins
--------

Plugin stuff



Publisher Subscriber model
---------------------------

Some cool pub/sub model docs



Environment configuration
---------------------------

More docs over env config