MASS Overview
===========================

This section contains information at a high level explaining Scenario
Studio and SCRIMMAGE. This will not get into specifics of how they work,
or what to do inside them. Instead, this will be an overview of why the
user should care about this information, and how to navigate through this
documentation.

Environment Variables
---------------------

SCRIMMAGE relies on several environment variables to find XML files, plugins,
3D mesh files, binary data files, and configuration files. The following is a
description of SCRIMMAGE's environment variables. We should not get into specifics
about how these work in this section however. This would be reserved for the 
concept documentation section.