Making a Grilled Cheese
=========================

Recipe Overview
----------------

Grilled cheese sandwiches taste great and are very fast to make.
Creating grilled cheese sandwiches is a skill that everyone should
have, since it contains ingredients that can be readily found in most
kitchens. In this tutorial, I will walk you through one example of
how you can make a grilled cheese sandwich.


What you need
--------------

In order to make this recipe, you will need the following ingredients:

* Bread
* Butter
* Cheese
* Ham (optional)


Alternative demonstration
----------------------------

Here is a video demonstration to explain the full process. [11]_

.. raw:: html

  <div style="text-align: center; margin-bottom: 2em;">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/88YovCsnMxs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>


Lower Level Explanation
------------------------

.. Tip:: You can use a stove as a replacement to a grill.

For this recipe, we will be spreading ``butter`` on our ``bread`` and cooking the 
bread over a grill. 

.. figure:: https://cdn.apartmenttherapy.info/image/upload/f_auto,q_auto:eco,c_fit,w_760,h_507/k%2FPhoto%2FTips%2F2019-05-The-One-Thing-Everyone-Should-Grill-First%2FOneThingEveryoneShouldGrillFirstOption1
  :align: center

  If you want N grilled cheese, place N*2 slices of bread on the grill [12]_

We will continue to cook the bread until it is slightly
brown, at which time we will place cheese on the bread. The ``cheese`` will
cook faster, and so they should be finished cooking at the same time
together. 


.. figure:: https://cdn.apartmenttherapy.info/image/upload/f_auto,q_auto:eco,c_fit,w_760,h_507/k%2FPhoto%2FTips%2F2019-05-The-One-Thing-Everyone-Should-Grill-First%2FOneThingEveryoneShouldGrillFirstOption2
  :align: center

  Make sure to add the cheese a little before the bread is finished [13]_

We can **optionally** add ``ham`` to the grilled cheese to turn it into a
melt sandwich. If you would like to add ham, feel free to add it at the same time
as the cheese.

.. Caution:: Make sure not to burn your bread.

That is all we need to do! Below illustrates the implementation of a grilled
cheese. Notice that it is important that at line #3 that we add the cheese so
it is not just toast!

.. code-block:: python
  :linenos:
  :emphasize-lines: 3

  def create_grilled_cheese(bread, cheese, isMeat):
      bread.partially_cook()
      bread.add(cheese)
      if(isMeat):
        bread.add(cheese)
      bread.finish_cook()


.. figure:: https://image.shutterstock.com/image-photo/grilled-cheese-sandwich-260nw-645742051.jpg
  :align: center

  This should be the end result! [14]_


Citations
----------

.. [11] Inspire to Cook. (2015, April). How To Make The Perfect Grilled Cheese Sandwich. Youtube. https://www.youtube.com/watch?v=88YovCsnMxs
.. [12] Westerhausen, Shelly. The One Thing Everyone Should Grill First Option 1. https://cdn.apartmenttherapy.info/image/upload/f_auto,q_auto:eco,c_fit,w_760,h_507/k%2FPhoto%2FTips%2F2019-05-The-One-Thing-Everyone-Should-Grill-First%2FOneThingEveryoneShouldGrillFirstOption1
.. [13] Westerhausen, Shelly. The One Thing Everyone Should Grill First Option 2. https://cdn.apartmenttherapy.info/image/upload/f_auto,q_auto:eco,c_fit,w_760,h_507/k%2FPhoto%2FTips%2F2019-05-The-One-Thing-Everyone-Should-Grill-First%2FOneThingEveryoneShouldGrillFirstOption2
.. [14] Dolgikh, G. (n.d.). Retrieved from https://www.shutterstock.com/image-photo/grilled-cheese-sandwich-645742051
