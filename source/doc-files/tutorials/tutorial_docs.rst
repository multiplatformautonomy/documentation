Tutorials
=====================

This section will cover specific examples on how to perform common tasks in
the Scenario Studio. Possible tasks that might appear in this section are:

* Navigating through the Scenario Studio via video demonstration (if Scenario Studio is not intuitive enough)
* Creating a flight scenario with two separate entities
* Creating a PyTorch model using the plugin
* Creating a Tensorflow model
* Creating an OpenAI model

This section is designed for first time users that are unfamiliar with 
SCRIMMAGE and the Scenario Studio. It provides a quick way to get caught
up on the functionalities, as well as to follow along so that users will
be less likely to be lost.
