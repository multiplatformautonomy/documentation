Resources
==========

This section will provide additional resources for users that want to learn more
about SCRIMMAGE and its dependencies.

See the `SCRIMMAGE README on GitHub`_ for documentation over SCRIMMAGE.

.. _SCRIMMAGE README on GitHub: https://github.com/gtri/scrimmage/blob/master/README.md